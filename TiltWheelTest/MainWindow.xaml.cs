﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TiltWheelTest
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        MouseHorizontalWheelService _horizontalWheelService;

        public MainWindow()
        {
            InitializeComponent();

            this.Title = "WM_MOUSEHWHEEL event capture ";
            WriteLine("Process="+(System.Environment.Is64BitProcess ? "64bit" : "32bit"));

            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _horizontalWheelService = new MouseHorizontalWheelService(this);
            _horizontalWheelService.MouseHorizontalWheelChanged += HorizontalWheelService_MouseHorizontalWheelChanged;
        }

        private void HorizontalWheelService_MouseHorizontalWheelChanged(object sender, MouseWheelEventArgsEx e)
        {
            var wParam = e.wParam.ToString("X");
            var lParam = e.lParam.ToString("X");
            WriteLine($"Time={e.Timestamp}, wParam={wParam}, lParam={lParam}, Delta={e.Delta}");
        }

        private void WriteLine(string s)
        {
            this.LogTextBox.AppendText(s + "\r\n");
            this.LogTextBox.ScrollToEnd();
        }
    }
}
