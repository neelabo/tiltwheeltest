﻿using System;
using System.Windows.Input;

namespace TiltWheelTest
{
    public class MouseWheelEventArgsEx : MouseWheelEventArgs
    {
        public MouseWheelEventArgsEx(MouseDevice mouse, int timestamp, int delta) : base(mouse, timestamp, delta)
        {
        }

        public IntPtr wParam { get; set; }
        public IntPtr lParam { get; set; }
    }

}
